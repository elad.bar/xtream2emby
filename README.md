# Xtream2Emby

Convert Xtream lists to STRM files and Live TV (including EPG), support multiple providers.

## How to install

### Prerequisites

- Xtream IPTV account

### Docker Compose

```yaml
version: "3"
services:
  xtream2emby:
    image: "registry.gitlab.com/elad.bar/xtream2emby:latest"
    container_name: "xtream2emby"
    hostname: "xtream2emby"
    restart: "unless-stopped"
    environment:
      - EXPORTER_PORT=9563
      - DEBUG=False
    volumes:
      - $PWD/config/config.json:/app/config/config.json
      - $PWD/media:/app/media
      - $PWD/cache:/app/cache
```

#### Image tags per architecture

- latest: linux/amd64
- arm32v7: linux/arm32/v7
- arm64v8: linux/arm64/v8

### Environment Variables

| Variable      | Default | Required | Description                  |
| ------------- | ------- | -------- | ---------------------------- |
| EXPORTER_PORT | 9563    | -        | Port for Prometheus exporter |
| DEBUG         | false   | -        | Enable debug log messages    |

### Configuration file

Configuration file must be located at `/config/config.json`

```json
{
  "IPTVProviderName": {
    "url": "http://provider.domain.com",
    "username": "username",
    "password": "password",
    "interval": 360,
    "category_name_regex": {
      "^[^\\|]*.[\\|].": "",
      "^[^\\-]*.[\\-].": ""
    },
    "settings": {
      "live": {
        "enabled": true,
        "use_server_info": false,
        "name_regex": {
          "^[^\\|]*.[\\|].": "",
          "^[^\\-]*.[\\-].": ""
        },
        "exclude_categories": null
      },
      "movie": {
        "enabled": true,
        "use_server_info": false,
        "category_folder": true,
        "name_regex": {
          "[A-Z]{2}.\\s": "",
          "\\s*$": "",
          "^[^\\|]*.[\\|].": "",
          "^[^\\-]*.[\\-].": ""
        },
        "exclude_categories": []
      },
      "series": {
        "enabled": true,
        "use_server_info": false,
        "category_folder": true,
        "name_regex": {
          "[A-Z]{2}.\\s": "",
          "\\s*$": "",
          "^[^\\|]*.[\\|].": "",
          "^[^\\-]*.[\\-].": ""
        },
        "exclude_categories": []
      }
    }
  }
}
```

#### Vendor configuration

| Key                 | Description                                                                      |
| ------------------- | -------------------------------------------------------------------------------- |
| url                 | URL provided by the IPTV provider                                                |
| username            | Account's username                                                               |
| password            | Account's password                                                               |
| interval            | Interval in minutes between scans                                                |
| category_name_regex | Regex matches for replacing text for category name as key (find) value (replace) |

#### Per stream type (live, movie, series) configuration

| Key                | Description                                                                                                                                                        |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| enabled            | Extracting streams is active or not                                                                                                                                |
| use_server_info    | Should the base url for stream taken from server info (data from authentication), default is `False` which will make the stream base url as configuration in `url` |
| name_regex         | Regex matches for replacing text for stream (live, movie, series) name as key (find) value (replace)                                                               |
| exclude_categories | List of categories comma separated to exclude from fetching process, format: ["1", "2", "3"]                                                                       |
| category_folder    | Create media within category folder or media type root folder, default is `True`, will create the media within category                                            |
