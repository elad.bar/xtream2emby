from datetime import datetime
import sys
from time import time

from common.media_type import MediaType
from common.xtream_endpoints import XtreamEndpoint

from .base_streams_handler import BaseStreamsHandler
from .file_manager import FileManager


class LiveStreamsHandler(BaseStreamsHandler):
    def __init__(
            self,
            app_config: dict,
            provider_config: dict,
            file_manager: FileManager
    ):
        super().__init__(app_config, provider_config, file_manager)

        self._epg_data: str | None = None

    @property
    def media_type(self) -> MediaType | None:
        return MediaType.LIVE

    def _initialize_streams_queue(self):
        pass

    def _process_streams(self):
        try:
            start_time = time()
            self.log_info("Loading live streams")

            live_streams_data = ["#EXTM3U"]

            for live_stream in self.streams:
                can_process = self._can_process(live_stream)

                self._set_metrics_data(live_stream, can_process)

                if can_process:
                    lines = self._process_live_stream(live_stream)

                    if lines is not None:
                        live_streams_data.extend(lines)

            m3u_content = "\r\n".join(live_streams_data)

            date = datetime.now()

            self._add_file(f"{self._media_dir}/live.m3u", m3u_content, date)
            self._add_file(f"{self._media_dir}/epg.xml", self._epg_data, date)

            execution_time = time() - start_time

            self.log_info(
                f"Processed live streams [{len(self.streams):,}], "
                f"Duration: {execution_time:.3f} seconds"
            )

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(
                f"Failed to load live streams, Error: {ex}, Line: {exc_tb.tb_lineno}"
            )

    def _set_metrics_data(self, live_stream: dict, can_process: bool):
        channel_category_id = live_stream.get("category_id")
        channel_group = self._categories.get(channel_category_id)

        stream_data = {
            "provider": self._provider_name,
            "type": self.media_type,
            "category": channel_group,
            "imported": can_process,
            "items": 1
        }

        if channel_group in self._metrics_data:
            metrics_data = self._metrics_data.get(channel_group)
            items = metrics_data.get("items", 0) + 1

            self._metrics_data[channel_group].update({
                "imported": can_process,
                "items": items
            })

        else:
            self._metrics_data[channel_group] = stream_data

    def _process_live_stream(self, live_stream: dict) -> list[str] | None:
        lines = None

        try:
            channel_name = live_stream.get("name")
            channel_category_id = live_stream.get("category_id")
            channel_group = self._categories.get(channel_category_id)

            channel_name = self._clean_name_regex(channel_name)

            channel_unique_id = live_stream.get("epg_channel_id")

            channel_number = live_stream.get("stream_id")
            channel_logo = live_stream.get("stream_icon")

            stream_type = live_stream.get("stream_type", MediaType.LIVE)

            stream_url = self._build_stream_url(channel_number, "m3u8")

            tags_info = {
                "name": channel_name,
                "id": channel_unique_id,
                "logo": channel_logo,
                "type": stream_type,
            }

            tags = [
                f'tvg-{key}="{tags_info[key]}"'
                for key in tags_info
                if tags_info[key] is not None
            ]

            if channel_group is not None:
                tags.append(f'group-title="{channel_group}"')
                tags.append(f'tag-group="{channel_group}"')

            lines = [f"#EXTINF:-1,{' '.join(tags)},{channel_name}", stream_url]

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(
                f"Failed to load stream lines, "
                f"Data: {live_stream}, "
                f"Error: {ex}, "
                f"Line: {exc_tb.tb_lineno}"
            )

        return lines

    def _get_data_points(self) -> list[list] | None:
        args = super()._get_data_points()
        args.append([XtreamEndpoint.EPG, XtreamEndpoint.EPG, None])

        return args

    def _extra_data_loading(self, endpoint: XtreamEndpoint, data: str | list | dict):
        if endpoint == XtreamEndpoint.EPG:
            self._epg_data = data
