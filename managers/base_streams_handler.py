from datetime import datetime
import json
import logging
from queue import Queue
import re
import sys
import threading
from time import sleep, time

from ratelimitqueue import ratelimitqueue
import requests

from common.consts import (
    CACHE_DIR,
    CLEAN_CHARS,
    CONTEXT_PARAMETER,
    HEADERS,
    LIMIT_PER_MEDIA,
    LOAD_DATA_ACTIONS,
    MEDIA_DIR,
    MEDIA_RESOLVER_CATEGORIES,
    MEDIA_RESOLVER_STREAMS,
    MEDIA_RESOLVERS,
    METRICS_ITEMS_LABELS,
    XTREAM_PASSWORD,
    XTREAM_SETTINGS,
    XTREAM_URL,
    XTREAM_USERNAME,
)
from common.media_type import MediaType
from common.xtream_actions import XtreamAction
from common.xtream_endpoints import XtreamEndpoint
from managers.file_manager import FileManager

_LOGGER = logging.getLogger(__name__)


class BaseStreamsHandler:
    def __init__(self,
                 app_config: dict,
                 provider_config: dict,
                 file_manager: FileManager
                 ):

        self._extract_only = app_config.get("extract_only")
        self._provider_name = app_config.get("name")

        all_metric_collectors = app_config.get("metrics")
        metric_collectors = all_metric_collectors.get(self.media_type)

        self._process_number = 0

        self._file_manager = file_manager
        self._metric_collectors = metric_collectors

        self._username: str = provider_config.get(XTREAM_USERNAME)
        self._password: str = provider_config.get(XTREAM_PASSWORD)
        self._provider_url: str = provider_config.get(XTREAM_URL)
        self._category_name_regex: dict[str, str] = provider_config.get("category_name_regex", {})

        self._cache_dir = f"{CACHE_DIR}/{self._provider_name}"
        self._media_dir = f"{MEDIA_DIR}/{self._provider_name}/{self.media_type}"

        settings: dict = provider_config.get(XTREAM_SETTINGS)

        self._settings = settings.get(self.media_type)
        self._use_server_info = self._settings.get("use_server_info", False)
        self._name_regex: dict[str, str] = self._settings.get("name_regex", {})
        self._exclude_categories: dict[str, str] = self._settings.get("exclude_categories", [])

        self._category_folder = self._settings.get("category_folder", True)

        if self._exclude_categories is None:
            self._exclude_categories = []

        self._resolvers = MEDIA_RESOLVERS.get(self.media_type)

        self._data = {}
        self._categories = {}
        self._files = {}

        self._server_info: dict | None = None
        self._is_authenticated: bool | None = None

        self._metrics_data = {}
        self._use_cache = False

        self._streams_count = 0
        self._processed_count = 0
        self._processing_start_time: float = 0

        self._limit_per_second = LIMIT_PER_MEDIA.get(self.media_type, 0)

        queue: Queue = Queue()

        if self._limit_per_second > 0:
            queue = ratelimitqueue.RateLimitQueue(calls=self._limit_per_second, per=1)

        self._streams_queue = queue

        self.is_enabled = self._settings.get("enabled", False)

    @property
    def media_type(self) -> MediaType | None:
        return None

    @property
    def streams(self) -> list:
        return self._data.get(MEDIA_RESOLVER_STREAMS)

    @property
    def categories(self) -> dict:
        return self._data.get(MEDIA_RESOLVER_CATEGORIES)

    def _reset_counters(self, streams: int):
        self._streams_count = streams
        self._processed_count = 0

    def _update_counters(self):
        execution_time = time() - self._processing_start_time

        self._processed_count += 1

        progress = self._processed_count / self._streams_count
        progress_left_ratio = 1 / progress
        expected_duration = progress_left_ratio * execution_time
        time_left = expected_duration - execution_time

        self.log_info(
            f"Processed {self._processed_count} / {self._streams_count} ({progress:.3%}), "
            f"Elapsed time: {time_left:.2f}"

        )

    def process(self):
        try:
            self._process_number += 1

            self._processing_start_time = time()

            self._load_data()
            self._load_categories()

            self.log_info("Loading streams")

            self._initialize_streams_queue()

            self._process_streams()

            self._report_metrics()

            execution_time = time() - self._processing_start_time

            self._report_processing_time_metrics(execution_time)

            self._files = {}
            self._data = {}
            self._categories = {}

            self.log_info(f"Complete processing, Duration: {execution_time:.3f} seconds")

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(f"Failed to process, Error: {ex}, Line: {exc_tb.tb_lineno}")

    def _initialize_streams_queue(self):
        self.log_info("Initializing streams queue")

        self._streams_queue.empty()

        processable_streams = [
            stream
            for stream in self.streams
            if self._can_process(stream)
        ]

        processable_streams_count = len(processable_streams)

        self.log_info(f"Available streams for processing: {processable_streams_count}")

        for stream in processable_streams:
            self._streams_queue.put(stream)

        self.streams.clear()
        del self._data[MEDIA_RESOLVER_STREAMS]

        self._reset_counters(processable_streams_count)

    def _process_streams(self):
        threads = []

        while self._streams_queue.qsize() > 0:
            stream = self._streams_queue.get()

            thread = threading.Thread(target=self._process_one, args=[stream])
            thread.start()

            threads.append(thread)

        for thread in threads:
            thread.join()

    def _process_one(self, stream: dict):
        try:
            self._process_item(stream)

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(
                f"Failed to process {self.media_type} stream, ID: {json.dumps(stream)}, Error: {ex}, Line: {exc_tb.tb_lineno}"
            )

        self._streams_queue.task_done()

        self._update_counters()

    def _process_item(self, stream: dict):
        pass

    def _can_process(self, stream_info: dict):
        stream_name = stream_info.get("name")
        category_id = stream_info.get("category_id")

        is_excluded = category_id in self._exclude_categories

        can_process = stream_name is not None and not is_excluded

        return can_process

    def set_provider_url(self, server_info: dict):
        url = server_info.get("url")
        server_protocol = server_info.get("server_protocol")

        self._provider_url = f"{server_protocol}://{url}"

    def _load_data(self):
        try:
            start_time = time()

            self._data = {}
            self._categories = {}

            all_data_points = self._get_data_points()

            for data_points in all_data_points:
                endpoint: XtreamEndpoint = data_points[0]
                data_point: str = data_points[1]
                action: XtreamAction | None = data_points[2]

                self._load_data_point(endpoint, data_point, action)

            execution_time = time() - start_time

            self.log_info(
                f"Loaded {len(all_data_points)} lists, Duration: {execution_time:.3f} seconds"
            )

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(f"Failed to load data, Error: {ex}, Line: {exc_tb.tb_lineno}")

    def _get_data_points(self) -> list[list] | None:
        args = []

        for resolver_action in LOAD_DATA_ACTIONS:
            action = self._resolvers.get(resolver_action)

            if action is not None:
                args.append([XtreamEndpoint.PLAYER, resolver_action, action])

        return args

    def _load_data_point(
            self,
            endpoint: XtreamEndpoint,
            data_point: str,
            action: XtreamAction | None = None
    ):
        try:
            self.log_debug(f"Load endpoint data, Endpoint: {endpoint}")

            data = self.get_data(endpoint, action)

            if data is not None:
                if endpoint == XtreamEndpoint.PLAYER:
                    self._data[data_point] = data

                self._extra_data_loading(endpoint, data)

                self.log_info(
                    f"Endpoint '{endpoint}' data loaded, Action: {data_point}"
                )

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(
                f"Failed to load endpoint data, "
                f"Endpoint: {endpoint}, "
                f"Error: {ex}, "
                f"Line: {exc_tb.tb_lineno}"
            )

    def _extra_data_loading(self, endpoint: XtreamEndpoint, data: str | list | dict):
        pass

    def _load_categories(self):
        try:
            start_time = time()
            self.log_info("Loading categories")

            for item in self.categories:
                category_id = item.get("category_id")
                category_name = item.get("category_name")

                clean_name = self._clean_category_name_regex(category_name)
                clean_name = self._clean_name(clean_name)

                self._categories[category_id] = clean_name.capitalize()

            execution_time = time() - start_time

            self.log_info(
                f"Loaded {len(self.categories)} lists, Duration: {execution_time:.3f} seconds"
            )

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(f"Failed to load data, Error: {ex}, Line: {exc_tb.tb_lineno}")

    @staticmethod
    def _clean_name(title) -> str:
        if title is not None:
            for key in CLEAN_CHARS:
                if key in title:
                    title = title.replace(key, CLEAN_CHARS[key])

        return title

    def _build_url(
        self,
        endpoint: XtreamEndpoint,
        action: XtreamAction | None = None,
        context_id: str | None = None,
    ) -> str:
        credentials = (
            f"{XTREAM_USERNAME}={self._username}&{XTREAM_PASSWORD}={self._password}"
        )
        url = f"{self._provider_url}/{endpoint}.php?{credentials}"

        if action is not None and endpoint == XtreamEndpoint.PLAYER:
            url = f"{url}&action={action}"

            if context_id is not None:
                context_key = CONTEXT_PARAMETER.get(action)

                if context_key is not None:
                    url = f"{url}&{context_key}={context_id}"

        self.log_debug(f"Built URL: {url}")

        return url

    def get_data(
        self,
        endpoint: XtreamEndpoint,
        action: XtreamAction | None = None,
        context_id: str | None = None
    ) -> str | list | dict | None:
        result = None
        path = self._get_cache_path(endpoint, action, context_id)

        if self._use_cache:
            result = self._file_manager.get(path)

        if result is None:
            url = self._build_url(endpoint, action, context_id)

            valid_response = False

            for attempt in range(0, 3):
                response = requests.get(url, headers=HEADERS)

                if response.ok:
                    valid_response = True
                    result = response.json() if endpoint.is_json() else response.text
                    break

                else:
                    sleep(1)

            if valid_response:
                sleep(0.1)

        if self._use_cache and result is not None:
            date = datetime.now().isoformat()
            self._file_manager.save(path, result, date)

        return result

    def _get_cache_path(self,
                        endpoint: XtreamEndpoint,
                        action: XtreamAction | None,
                        context_id: str | None) -> str | None:

        ext = endpoint.get_ext()

        parts = [endpoint, action, context_id, ext]
        relevant_parts = [
            str(part)
            for part in parts
            if part is not None
        ]

        file_name = ".".join(relevant_parts)

        path = f"{self._cache_dir}/{file_name}"

        return path

    def _build_stream_url(self, stream_id: str, ext: str) -> str:
        resource = f"{stream_id}.{ext}"

        url_parts = [
            self._provider_url,
            self.media_type,
            self._username,
            self._password,
            resource
        ]

        url = "/".join(url_parts)

        return url

    def _clean_category_name_regex(self, text) -> str:
        if text is not None:
            for regex_find in self._category_name_regex:
                regex_replace = self._category_name_regex[regex_find]

                text_new = re.sub(regex_find, regex_replace, text)

                text_new = text_new.rstrip().lstrip()

                if text != text_new:
                    text = text_new

        return text

    def _clean_name_regex(self, text) -> str | None:
        if text is not None:
            for regex_find in self._name_regex:
                regex_replace = self._name_regex[regex_find]

                text = re.sub(regex_find, regex_replace, text)

            text = text.lstrip().rstrip()

        return text

    def _add_file(self, file_path: str, content: str | dict | list, date: datetime):
        self._file_manager.save(file_path, content, date.isoformat())

    def log_error(self, message: str):
        _LOGGER.error(f"[{self._provider_name}::{self.media_type}] {message}")

    def log_warning(self, message: str):
        _LOGGER.warning(f"[{self._provider_name}::{self.media_type}] {message}")

    def log_info(self, message: str):
        _LOGGER.info(f"[{self._provider_name}::{self.media_type}] {message}")

    def log_debug(self, message: str):
        _LOGGER.debug(f"[{self._provider_name}::{self.media_type}] {message}")

    def _report_metrics(self):
        self.log_info("Report metrics")
        start_time = time()

        items_metric = self._metric_collectors.get("items")
        metrics_count = 0

        for metric_key in self._metrics_data:
            metrics_data = self._metrics_data.get(metric_key)
            metrics_value = metrics_data.get("items", 0)
            metrics_labels = []

            for metrics_label_name in METRICS_ITEMS_LABELS:
                metrics_label = metrics_data.get(metrics_label_name)
                metrics_labels.append(metrics_label)

            items_metric.labels(*metrics_labels).set(metrics_value)

            metrics_count += 1

        for key in self._metrics_data:
            self._metrics_data[key].update({
                "items": 0
            })

        execution_time = time() - start_time

        self.log_info(
            f"Reported {metrics_count:,} metrics, Duration: {execution_time:.3f} seconds"
        )

    def _report_processing_time_metrics(self, processing_time):
        processing_time_metric = self._metric_collectors.get("processing_time")
        metrics_labels = [self._provider_name, self.media_type]

        processing_time_metric.labels(*metrics_labels).set(processing_time)

    def _set_metrics_data(self, stream: dict, can_process: bool):
        pass
