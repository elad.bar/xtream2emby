from datetime import datetime

from common.media_type import MediaType

from .base_streams_handler import BaseStreamsHandler
from .file_manager import FileManager


class VODStreamsHandler(BaseStreamsHandler):
    def __init__(
            self,
            app_config: dict,
            provider_config: dict,
            file_manager: FileManager
    ):

        super().__init__(app_config, provider_config, file_manager)

    @property
    def media_type(self) -> MediaType | None:
        return MediaType.MOVIE

    def _process_item(self, stream):
        self._process_movie_stream(stream)

    def _process_movie_stream(self, movie_stream: dict):
        movie_name = movie_stream.get("name")
        added = movie_stream.get("added")
        category_id = movie_stream.get("category_id")

        movie_name = self._clean_name_regex(movie_name)

        movie_id = movie_stream.get("stream_id")
        container_extension = movie_stream.get("container_extension")
        movie_category = self._categories.get(category_id)

        movie_name_clean = self._clean_name(movie_name)

        base_file_path_parts = [
            f"{self._media_dir}s"
        ]

        if self._category_folder:
            base_file_path_parts.append(movie_category)

        base_file_path_parts.append(movie_name_clean)
        base_file_path_parts.append(movie_name_clean)

        base_file_path = "/".join(base_file_path_parts)

        stream_file = f"{base_file_path}.strm"
        stream_url = self._build_stream_url(movie_id, container_extension)

        stream_data_file = f"{base_file_path}.json"

        date = datetime.fromtimestamp(int(added))

        self._add_file(stream_file, stream_url, date)
        self._add_file(stream_data_file, movie_stream, date)

    def _set_metrics_data(self, movie_stream: dict, can_process: bool):
        category_id = movie_stream.get("category_id")
        movie_category = self._categories.get(category_id)

        stream_data = {
            "provider": self._provider_name,
            "type": self.media_type,
            "category": movie_category,
            "imported": can_process,
            "items": 1
        }

        if movie_category in self._metrics_data:
            metrics_data = self._metrics_data.get(movie_category)
            streams = metrics_data.get("items", 0) + 1

            self._metrics_data[movie_category].update({
                "imported": can_process,
                "items": streams
            })

        else:
            self._metrics_data[movie_category] = stream_data
