from datetime import datetime
import sys

from common.media_type import MediaType
from common.xtream_actions import XtreamAction
from common.xtream_endpoints import XtreamEndpoint

from .base_streams_handler import BaseStreamsHandler
from .file_manager import FileManager


class SeriesStreamsHandler(BaseStreamsHandler):
    def __init__(self,
                 app_config: dict,
                 provider_config: dict,
                 file_manager: FileManager
                 ):

        super().__init__(app_config, provider_config, file_manager)

    @property
    def media_type(self) -> MediaType | None:
        return MediaType.SERIES

    def _process_item(self, stream):
        self._process_series_stream(stream)

    def _get_stream_info_path(self, stream: dict) -> str:
        series_name = stream.get("name")
        category_id = stream.get("category_id")

        series_category = self._categories.get(category_id)

        series_name = self._clean_name_regex(series_name)
        series_name_clean = self._clean_name(series_name)

        base_path_parts = [
            self._media_dir
        ]

        if self._category_folder:
            base_path_parts.append(series_category)

        base_path_parts.append(series_name_clean)
        base_path = "/".join(base_path_parts)

        stream_data_file = f"{base_path}/{series_name_clean}.json"

        return stream_data_file

    def _process_series_stream(self, stream: dict):
        try:
            series_id = stream.get("series_id")

            self.log_debug(f"Updating stream for #{series_id}")

            data = self.get_data(
                XtreamEndpoint.PLAYER,
                XtreamAction.SERIES_INFO,
                series_id
            )

            if data is not None:
                stream.update(data)
                stream_info = stream.get("info")

                added = stream_info.get("last_modified")

                date = datetime.fromtimestamp(int(added))

                stream_info_path = self._get_stream_info_path(stream)

                self._add_file(stream_info_path, stream, date)

                base_path_parts = stream_info_path.split("/")[:-1]
                base_path = "/".join(base_path_parts)

                episodes_data = stream.get("episodes", {})

                for season in episodes_data:
                    season_data = episodes_data[season]

                    for episode in season_data:
                        self._process_episode(base_path, episode)

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(
                f"Failed to process stream, "
                f"Stream: {stream}, "
                f"Error: {ex}, "
                f"Line: {exc_tb.tb_lineno}"
            )

    def _process_episode(self, base_path: str, episode: dict):
        base_path_parts = base_path.split("/")
        series_name_clean = base_path_parts[len(base_path_parts) - 1]

        try:
            stream_id = episode.get("id")
            episode_number = episode.get("episode_num")
            season_number = episode.get("season")
            container_extension = episode.get("container_extension")
            added = episode.get("added")

            season_pad = str(season_number).rjust(2, "0")
            episode_short = str(episode_number).rjust(2, "0")

            episode_file = f"{series_name_clean} - S{season_pad}E{episode_short}"

            season_dir = f"Season {season_pad}"

            episode_file = (
                f"{base_path}/{season_dir}/{episode_file}.strm"
            )

            episode_stream_url = self._build_stream_url(
                stream_id, container_extension
            )

            date = datetime.fromtimestamp(int(added))

            self._add_file(episode_file, episode_stream_url, date)

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()

            self.log_error(
                "Failed to process series, "
                f"Series: {series_name_clean}, "
                f"Episode: {episode}, "
                f"Error: {ex}, "
                f"Line: {exc_tb.tb_lineno}"
            )

    def _set_metrics_data(self, series_info: dict, can_process: bool):
        category_id = series_info.get("category_id")
        series_category = self._categories.get(category_id)

        stream_data = {
            "provider": self._provider_name,
            "type": self.media_type,
            "category": series_category,
            "imported": can_process,
            "items": 1
        }

        if series_category in self._metrics_data:
            metrics_data = self._metrics_data.get(series_category)
            items = metrics_data.get("items", 0) + 1

            self._metrics_data[series_category].update({
                "imported": can_process,
                "items": items
            })

        else:
            self._metrics_data[series_category] = stream_data
