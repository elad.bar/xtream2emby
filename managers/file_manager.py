import hashlib
import json
import os
from pathlib import Path

from common.consts import CACHE_DIR, DEFAULT_FILE_CONTENT, ENCODING_UTF8


class FileManager:
    def __init__(self, provider_name: str):
        self._file_path = f"{CACHE_DIR}/{provider_name}/files.json"

        self._files_db: dict[str, dict] | None = None

    def reset(self):
        files_db = self.get(self._file_path)

        if files_db is not None:
            self._files_db = files_db

    def update_db(self):
        path = self._file_path
        file_path_parts = path.split("/")
        directory_path = "/".join(file_path_parts[:-1])

        self.prepare_directory(directory_path)

        with open(path, "w", encoding=ENCODING_UTF8) as f:

            f.write(json.dumps(self._files_db, indent=4))

        self._files_db = {}

    def save(self, path: str, content: str | list | dict | None, date: str):
        content_str = json.dumps(content)
        content_bytes = content_str.encode(ENCODING_UTF8)
        content_hash = hashlib.md5(content_bytes).hexdigest()

        file_history = self._files_db.get(path, {})
        item_hash = file_history.get("hash")

        if item_hash != content_hash:
            file_path_parts = path.split("/")
            directory_path = "/".join(file_path_parts[:-1])

            self.prepare_directory(directory_path)

            file_content = content

            if path.endswith(".json"):
                file_content = json.dumps(content, indent=4)

            with open(path, "w", encoding=ENCODING_UTF8) as f:
                f.write(file_content)

            self._files_db[path] = {
                "hash": content_hash,
                "added": date
            }

    @staticmethod
    def prepare_directory(directory_path):
        if not os.path.exists(directory_path):
            Path(directory_path).mkdir(parents=True, exist_ok=True)

    @staticmethod
    def get(path: str, default_value: str | list | dict | None = None) -> str | list | dict | None:
        result: str | list | dict | None = default_value if default_value is not None else DEFAULT_FILE_CONTENT

        if os.path.exists(path):
            with open(path, encoding=ENCODING_UTF8) as f:
                result = f.read()

                if path.endswith(".json"):
                    result = json.loads(result)

        return result
