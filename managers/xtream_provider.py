from datetime import datetime
import logging
import threading
from time import sleep

from prometheus_client import Gauge
import requests

from common.consts import (
    CACHE_DIR,
    DEFAULT_SCAN_INTERVAL,
    XTREAM_PASSWORD,
    XTREAM_SCAN_INTERVAL,
    XTREAM_USERNAME,
)
from common.media_type import MediaType
from common.xtream_endpoints import XtreamEndpoint

from .file_manager import FileManager
from .live_streams_handler import LiveStreamsHandler
from .series_streams_handler import SeriesStreamsHandler
from .vod_streams_handler import VODStreamsHandler

_LOGGER = logging.getLogger(__name__)


class XtreamProvider:
    def __init__(self,
                 provider_name: str,
                 config: dict,
                 metrics: dict[MediaType, dict[str, Gauge]],
                 extract_only: bool):

        _LOGGER.info(f"[{provider_name}] Starting")
        self._provider_name = provider_name

        emby_config = config.get("libraryRefresh")

        emby_protocol: str = emby_config.get("protocol")
        emby_hostname: str = emby_config.get("hostname")
        emby_port: str = emby_config.get("port")

        self._emby_token: str = emby_config.get("token")
        self._post_processing_enabled: bool = config.get("enabled", False)

        self._post_processing_url: str = f"{emby_protocol}://{emby_hostname}:{emby_port}"

        self._username: str = config.get(XTREAM_USERNAME)
        self._password: str = config.get(XTREAM_PASSWORD)
        self._execution_details_path = f"{CACHE_DIR}/{self._provider_name}/execution.json"

        self._scan_interval = int(
            str(config.get(XTREAM_SCAN_INTERVAL, DEFAULT_SCAN_INTERVAL))
        )

        self._is_ready = self._username is not None and self._password is not None

        self._file_manager = FileManager(self._provider_name)

        provider_config = config
        app_config = {
            "name": provider_name,
            "extract_only": extract_only,
            "metrics": metrics
        }

        self._stream_handlers = [
            LiveStreamsHandler(app_config, provider_config, self._file_manager),
            SeriesStreamsHandler(app_config, provider_config, self._file_manager),
            VODStreamsHandler(app_config, provider_config, self._file_manager),
        ]

    def initialize(self):
        if self._is_ready:
            self._start_processing()

        else:
            _LOGGER.error(f"[{self._provider_name}] Failed to initialize process, Please set credentials")

    def _authenticate(self):
        is_auth = False

        first_handler = self._stream_handlers[0]
        data = first_handler.get_data(XtreamEndpoint.PLAYER)

        if data is not None:
            server_info = data.get("server_info")

            user_info = data.get("user_info")
            authenticated = user_info.get("auth")
            status = user_info.get("status")

            is_auth = authenticated == 1 and status == "Active"

            if is_auth:
                for stream_handler in self._stream_handlers:
                    stream_handler.set_provider_url(server_info)

        if not is_auth:
            raise ConnectionError()

    def _start_processing(self):
        _LOGGER.info(f"[{self._provider_name}] Initializing process")

        while True:
            try:
                self._authenticate()
                self._file_manager.reset()

                threads = []
                handlers = [
                    handler
                    for handler in self._stream_handlers
                    if handler.is_enabled
                ]

                for stream_handler in handlers:
                    thread = threading.Thread(
                        target=stream_handler.process
                    )
                    threads.append(thread)
                    thread.start()

                for thread in threads:
                    thread.join()

                self._file_manager.update_db()
                self._post_processing()

                _LOGGER.info(f"{self._provider_name} processing completed")

            except ConnectionError:
                _LOGGER.error(f"Failed to start processing, Provider: {self._provider_name}, Error: Invalid Credentials")

            except Exception as ex:
                _LOGGER.error(f"Failed to start processing, Provider: {self._provider_name}, Error: {ex}")

            self.wait_for_next_iteration()

    def wait_for_next_iteration(self):
        now = datetime.now().timestamp()
        next_interval = 60 * self._scan_interval
        next_iteration = now + next_interval
        next_iteration_iso = datetime.fromtimestamp(next_iteration).isoformat()

        _LOGGER.info(f"Next iteration for {self._provider_name} at {next_iteration_iso}")

        sleep(next_interval)

    def _post_processing(self):
        if self._post_processing_enabled:
            url = f"{self._post_processing_url}/Library/Refresh"
            server_details = f"Emby Server: {self._post_processing_url}"
            headers = {
                "X-Emby-Token": self._emby_token
            }

            response = requests.post(url, headers=headers)

            if response.ok:
                _LOGGER.info(f"Refresh library triggered, {server_details}")

            else:
                _LOGGER.error(
                    f"Refresh library failed to trigger, {server_details}, "
                    f"Error: {response.status_code}"
                )
