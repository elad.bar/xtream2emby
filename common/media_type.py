from enum import StrEnum


class MediaType(StrEnum):
    LIVE = "live"
    SERIES = "series"
    MOVIE = "movie"
