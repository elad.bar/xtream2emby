from common.media_type import MediaType
from common.xtream_actions import XtreamAction
from common.xtream_endpoints import XtreamEndpoint

LOG_FORMAT = "%(asctime)s %(levelname)s %(name)s %(message)s"
ENV_DEBUG = "DEBUG"
ENV_EXTRACT_ONLY = "EXTRACT_ONLY"

CONFIG_FILE = "config/config.json"

XTREAM_USERNAME = "username"
XTREAM_PASSWORD = "password"
XTREAM_URL = "url"
XTREAM_SCAN_INTERVAL = "interval"
XTREAM_SETTINGS = "settings"

DEFAULT_FILE_CONTENT: dict = {}

LIMIT_PER_MEDIA = {
    MediaType.SERIES: 40
}

DEFAULT_SCAN_INTERVAL = 60 * 6
MAX_THREADS = 5

USE_CACHE = False

CACHE_DIR = "cache"
MEDIA_DIR = "media"

ENCODING_UTF8 = "utf-8"

CLEAN_CHARS = {"&": "and", ":": "", "?": "", "/": "-", "*": "_", '"': "'", "|": "-", "\t": ""}

CONTEXT_PARAMETER = {
    XtreamAction.SERIES_INFO: "series_id",
    XtreamAction.VOD_INFO: "vod_id",
    XtreamAction.EPG_INFO: "stream_id"
}

SPECIAL_STREAM_TYPE = {
    "radio_streams": MediaType.LIVE
}

XTREAM_METADATA_TYPE = {
    XtreamEndpoint.PLAYER: "json",
    XtreamEndpoint.EPG: "xml"
}

MEDIA_RESOLVER_CATEGORIES = "categories"
MEDIA_RESOLVER_STREAMS = "streams"
MEDIA_RESOLVER_INFO = "info"

LOAD_DATA_ACTIONS = [
    MEDIA_RESOLVER_STREAMS,
    MEDIA_RESOLVER_CATEGORIES
]

MEDIA_RESOLVERS = {
    MediaType.LIVE: {
        MEDIA_RESOLVER_STREAMS: XtreamAction.LIVE_STREAMS,
        MEDIA_RESOLVER_CATEGORIES: XtreamAction.LIVE_CATEGORIES,
        MEDIA_RESOLVER_INFO: XtreamAction.EPG_INFO
    },
    MediaType.MOVIE: {
        MEDIA_RESOLVER_STREAMS: XtreamAction.VOD_STREAMS,
        MEDIA_RESOLVER_CATEGORIES: XtreamAction.VOD_CATEGORIES,
        MEDIA_RESOLVER_INFO: XtreamAction.VOD_INFO
    },
    MediaType.SERIES: {
        MEDIA_RESOLVER_STREAMS: XtreamAction.SERIES_STREAMS,
        MEDIA_RESOLVER_CATEGORIES: XtreamAction.SERIES_CATEGORIES,
        MEDIA_RESOLVER_INFO: XtreamAction.SERIES_INFO
    },
}

DEFAULT_USER_AGENT = (
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
    "AppleWebKit/537.36 (KHTML, like Gecko) "
    "Chrome/117.0.0.0 Safari/537.36"
)

HEADERS = {
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": DEFAULT_USER_AGENT,
}

METRICS_PROCESSING_TIME_LABELS = ["provider", "category"]
METRICS_ITEMS_LABELS = [*METRICS_PROCESSING_TIME_LABELS, "type", "imported"]

METRIC_LABELS = [
    MediaType.LIVE,
    MediaType.MOVIE,
    MediaType.SERIES
]
