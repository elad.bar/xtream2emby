from enum import StrEnum


class XtreamEndpoint(StrEnum):
    PLAYER = "player_api"
    EPG = "xmltv"

    def is_json(self) -> bool:
        return self == XtreamEndpoint.PLAYER

    def get_ext(self) -> str:
        return "json" if self == XtreamEndpoint.PLAYER else "xml"
