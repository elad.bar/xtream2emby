#!/usr/bin/env python3
import json
import logging
import os
import sys
import threading

from prometheus_client import CollectorRegistry, Gauge, start_http_server

from common.consts import (
    CONFIG_FILE,
    ENCODING_UTF8,
    ENV_DEBUG,
    ENV_EXTRACT_ONLY,
    LOG_FORMAT,
    MEDIA_RESOLVERS,
    METRICS_ITEMS_LABELS,
    METRICS_PROCESSING_TIME_LABELS,
)
from common.media_type import MediaType
from managers.xtream_provider import XtreamProvider

DEBUG = str(os.environ.get(ENV_DEBUG, False)).lower() == str(True).lower()

log_level = logging.DEBUG if DEBUG else logging.INFO

root = logging.getLogger()
root.setLevel(log_level)

stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(log_level)
formatter = logging.Formatter(LOG_FORMAT)
stream_handler.setFormatter(formatter)
root.addHandler(stream_handler)

logging.getLogger("urllib3").setLevel(logging.WARNING)

_LOGGER = logging.getLogger(__name__)


class EntrypointService:
    def __init__(self):
        self._registry: CollectorRegistry | None = None
        self._metrics: dict[MediaType, dict[str, Gauge]] = {}
        self._extract_only = (
            str(os.environ.get(ENV_EXTRACT_ONLY, False)).lower() == str(True).lower()
        )

    def initialize(self):
        config = {}
        threads = []

        _LOGGER.info("Initializing Xtream2Emby")

        exporter_port = int(os.environ.get("EXPORTER_PORT", "9563"))
        start_http_server(exporter_port)

        self._registry = CollectorRegistry()

        item_metrics = Gauge("xtream_items", "Xtream Items", METRICS_ITEMS_LABELS)

        self._registry.register(item_metrics)

        processing_time_metrics = Gauge(
            "xtream_provider_processing_time",
            "Xtream Provider Processing Time",
            METRICS_PROCESSING_TIME_LABELS,
        )

        self._registry.register(processing_time_metrics)

        for media_type in MEDIA_RESOLVERS.keys():
            self._metrics[media_type] = {
                "items": item_metrics,
                "processing_time": processing_time_metrics,
            }

        if os.path.exists(CONFIG_FILE):
            with open(CONFIG_FILE, encoding=ENCODING_UTF8) as f:
                config = json.loads(f.read())

        for provider_name in config:
            provider_config = config.get(provider_name)

            thread = threading.Thread(
                target=self._add_provider,
                args=[provider_name, provider_config],
            )
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

        _LOGGER.info("Xtream2Emby stopped")

    def _add_provider(self, provider_name: str, config: dict):
        provider = XtreamProvider(
            provider_name, config, self._metrics, self._extract_only
        )
        provider.initialize()


service = EntrypointService()
service.initialize()
