FROM python:3.12-alpine
MAINTAINER Elad Bar <elad.bar@hotmail.com>

WORKDIR /app

COPY . ./

RUN apk update && \
    apk upgrade && \
    apk add curl && \
    pip install -r /app/requirements.txt

ENV EXPORTER_PORT=9563
ENV DEBUG=false
ENV EXTRACT_ONLY=false

RUN chmod +x /app/entrypoint.py

EXPOSE ${EXPORTER_PORT}

ENTRYPOINT ["python3", "/app/entrypoint.py"]
